from setuptools import setup

setup(
    name='mylibrary',
    version='0.1',
    description='My first Python library',
    author='Ammar Memari',
    packages=['mylibrary'],
    install_requires=[
        'numpy>=1.0',
    ],
)
