"""Bu modül topllam ve çarpım islemı içindir."""
def add_numbers(var1, var2):
    """
    Returns the sum of two  number.
    Args:
    var1: The first number.
    var2: The second number.
    Returns:
    Sum of var1 and var2.
         """
    return var1 + var2


def multiply_numbers(var1, var2):
    """
    Returns the multiplication of two number.
    Args:
    var1: The first number.
    var2: The second number
    
    Returns:
    Multiplication of 2 number.
      """
    return var1 * var2
python setup.py sdist bdist_wheel
pip install /mylibrary/src/mylibrary-0.1.tar.gz
