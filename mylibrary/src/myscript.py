"""mylibrary üzerinden değerlendirme yapma ve numpy metodu kullanarak sıralama"""
import numpy as np
import mylibrary

result = mylibrary.add_numbers(2, 3)
print("var1 + var2 = ", result)

result = mylibrary.multiply_numbers(2558, 374)
print("var1 * var2 = ", result)

var1 = np.array([1, 2])
var2= np.array([3, 4])
var3 = mylibrary.add_arrays(var1, var2)

print(var3)
