import sys
sys.path.append('/path/to')
import mylibrary 
def test_add():
    """test_add(): Moduldür
    assert: Doğru yanlış tsti 
    add_numbers(): Toplama işlemi için kontrol eder """
    assert add_numbers(2, 3) == 5
    assert add_numbers(-1, 1) == 0
    assert add_numbers(-1, -1) == -2
    assert add_numbers(0, 0) == 0
def test_multiply():
    """test_multiply(): Moduldür
    multiply_numbers(): Çarpma işlemi için kontrol eder """
    assert multiply_numbers(2, 3) == 6
    assert multiply_numbers(-1, 1) == -1
    assert multiply_numbers(-1, -1) == 1
    assert multiply_numbers(0, 0) == 0  
def test_array():
    """
    test_array(): Moduldür
    add_arrays(): İki dizi elemanlarını toplar
    """
    assert add_arrays([1, 2], [3, 4]) == [4, 6]
    assert add_arrays([5, 1, 7], [3, -1, 5]) == [8, 0, 12]
    assert add_arrays([1], [2]) == [3]
